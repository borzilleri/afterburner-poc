package com.example;

import com.fasterxml.jackson.annotation.JsonIgnore;

public interface Mixin {
	String getValue();

	void setValue(String v);

	class MixinImpl implements Mixin {
		private String value = "default value";

		@Override
		public String getValue() {
			return value;
		}

		@Override
		public void setValue(String v) {
			value = v;
		}
	}

	interface MixinView extends Mixin {
		@JsonIgnore
		Mixin getMixinImpl();

		@Override
		default String getValue() {
			return getMixinImpl().getValue();
		}

		@Override
		default void setValue(String v) {
			getMixinImpl().setValue(v);
		}
	}
}
