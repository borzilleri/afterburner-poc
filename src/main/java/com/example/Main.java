package com.example;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.module.afterburner.AfterburnerModule;

import java.io.IOException;

class Main {
	static void defaultObject() {
		System.out.println("Default Model:");
		Model model = new Model();
		System.out.println("Model: " + model.toString());
	}

	static void deserialize(ObjectMapper mapper, String js) {
		try {
			JsonNode node = mapper.readTree(js);
			System.out.println("JsonNode: " + node.toString());

			Model model = mapper.treeToValue(node, Model.class);
			System.out.println("Model: " + model.toString());
		}
		catch( IOException e ) {
			e.printStackTrace();
		}
	}

	static void withAfterburner(String js) {
		System.out.println("Deserialized with Afterburner:");

		ObjectMapper mapper = new ObjectMapper();
		mapper.registerModule(new AfterburnerModule());

		deserialize(mapper, js);
	}

	static void withoutAfterburner(String js) {
		System.out.println("Deserialized WITHOUT Afterburner:");
		ObjectMapper mapper = new ObjectMapper();
		deserialize(mapper, js);
	}


	public static void main(String[] args) {
		String json = "{\"name\": \"custom name\", \"value\": \"custom value\"}";
		defaultObject();
		withoutAfterburner(json);
		withAfterburner(json);
	}
}
