package com.example;

public class Model implements Mixin.MixinView {
	private Mixin mixinImpl = new MixinImpl();

	public String name = "default name";

	@Override
	public Mixin getMixinImpl() {
		return mixinImpl;
	}

	@Override
	public String toString() {
		return "name: " + name + ", value: " + getValue();
	}
}